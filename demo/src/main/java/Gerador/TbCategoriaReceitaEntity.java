package Gerador;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tb_categoria_receita", schema = "cook_system", catalog = "postgres")
public class TbCategoriaReceitaEntity {
    private long idCategoriareceita;
    private String descricao;
    private Collection<TbReceitaEntity> tbReceitasByIdCategoriareceita;

    @Id
    @Column(name = "id_categoriareceita", nullable = false)
    public long getIdCategoriareceita() {
        return idCategoriareceita;
    }

    public void setIdCategoriareceita(long idCategoriareceita) {
        this.idCategoriareceita = idCategoriareceita;
    }

    @Basic
    @Column(name = "descricao", nullable = true, length = 255)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbCategoriaReceitaEntity that = (TbCategoriaReceitaEntity) o;

        if (idCategoriareceita != that.idCategoriareceita) return false;
        if (descricao != null ? !descricao.equals(that.descricao) : that.descricao != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idCategoriareceita ^ (idCategoriareceita >>> 32));
        result = 31 * result + (descricao != null ? descricao.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tbCategoriaReceitaByIdCategoriareceita")
    public Collection<TbReceitaEntity> getTbReceitasByIdCategoriareceita() {
        return tbReceitasByIdCategoriareceita;
    }

    public void setTbReceitasByIdCategoriareceita(Collection<TbReceitaEntity> tbReceitasByIdCategoriareceita) {
        this.tbReceitasByIdCategoriareceita = tbReceitasByIdCategoriareceita;
    }
}
