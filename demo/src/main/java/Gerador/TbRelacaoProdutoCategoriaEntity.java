package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_relacao_produto_categoria", schema = "cook_system", catalog = "postgres")
@IdClass(TbRelacaoProdutoCategoriaEntityPK.class)
public class TbRelacaoProdutoCategoriaEntity {
    private int idCategoria;
    private int idProduto;

    @Id
    @Column(name = "id_categoria", nullable = false)
    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Id
    @Column(name = "id_produto", nullable = false)
    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbRelacaoProdutoCategoriaEntity that = (TbRelacaoProdutoCategoriaEntity) o;

        if (idCategoria != that.idCategoria) return false;
        if (idProduto != that.idProduto) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategoria;
        result = 31 * result + idProduto;
        return result;
    }
}
