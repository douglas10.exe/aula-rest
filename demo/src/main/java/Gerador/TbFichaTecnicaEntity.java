package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_ficha_tecnica", schema = "cook_system", catalog = "postgres")
public class TbFichaTecnicaEntity {
    private int idFichatecnica;
    private int rendimento;
    private int idUnidademedida;
    private int idCategoriareceita;
    private TbUnidadeMedidaEntity tbUnidadeMedidaByIdUnidademedida;

    @Id
    @Column(name = "id_fichatecnica", nullable = false)
    public int getIdFichatecnica() {
        return idFichatecnica;
    }

    public void setIdFichatecnica(int idFichatecnica) {
        this.idFichatecnica = idFichatecnica;
    }

    @Basic
    @Column(name = "rendimento", nullable = false)
    public int getRendimento() {
        return rendimento;
    }

    public void setRendimento(int rendimento) {
        this.rendimento = rendimento;
    }

    @Basic
    @Column(name = "id_unidademedida", nullable = false)
    public int getIdUnidademedida() {
        return idUnidademedida;
    }

    public void setIdUnidademedida(int idUnidademedida) {
        this.idUnidademedida = idUnidademedida;
    }

    @Basic
    @Column(name = "id_categoriareceita", nullable = false)
    public int getIdCategoriareceita() {
        return idCategoriareceita;
    }

    public void setIdCategoriareceita(int idCategoriareceita) {
        this.idCategoriareceita = idCategoriareceita;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbFichaTecnicaEntity that = (TbFichaTecnicaEntity) o;

        if (idFichatecnica != that.idFichatecnica) return false;
        if (rendimento != that.rendimento) return false;
        if (idUnidademedida != that.idUnidademedida) return false;
        if (idCategoriareceita != that.idCategoriareceita) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idFichatecnica;
        result = 31 * result + rendimento;
        result = 31 * result + idUnidademedida;
        result = 31 * result + idCategoriareceita;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_unidademedida", referencedColumnName = "id_unidademedida", nullable = false)
    public TbUnidadeMedidaEntity getTbUnidadeMedidaByIdUnidademedida() {
        return tbUnidadeMedidaByIdUnidademedida;
    }

    public void setTbUnidadeMedidaByIdUnidademedida(TbUnidadeMedidaEntity tbUnidadeMedidaByIdUnidademedida) {
        this.tbUnidadeMedidaByIdUnidademedida = tbUnidadeMedidaByIdUnidademedida;
    }
}
