package Gerador;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tb_categoria_produto", schema = "cook_system", catalog = "postgres")
public class TbCategoriaProdutoEntity {
    private int idCategoriaproduto;
    private String descrição;
    private Collection<TbProdutoEntity> tbProdutosByIdCategoriaproduto;

    @Id
    @Column(name = "id_categoriaproduto", nullable = false)
    public int getIdCategoriaproduto() {
        return idCategoriaproduto;
    }

    public void setIdCategoriaproduto(int idCategoriaproduto) {
        this.idCategoriaproduto = idCategoriaproduto;
    }

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbCategoriaProdutoEntity that = (TbCategoriaProdutoEntity) o;

        if (idCategoriaproduto != that.idCategoriaproduto) return false;
        if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategoriaproduto;
        result = 31 * result + (descrição != null ? descrição.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tbCategoriaProdutoByIdCategoriaProduto")
    public Collection<TbProdutoEntity> getTbProdutosByIdCategoriaproduto() {
        return tbProdutosByIdCategoriaproduto;
    }

    public void setTbProdutosByIdCategoriaproduto(Collection<TbProdutoEntity> tbProdutosByIdCategoriaproduto) {
        this.tbProdutosByIdCategoriaproduto = tbProdutosByIdCategoriaproduto;
    }
}
