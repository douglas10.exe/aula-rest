package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_receita_ingrediente", schema = "cook_system", catalog = "postgres")
public class TbReceitaIngredienteEntity {
    private long idReceitaIngrediente;
    private int idReceita;
    private long idProduto;
    private long quantidade;
    private String medidaCaseira;
    private TbReceitaEntity tbReceitaByIdReceita;
    private TbProdutoEntity tbProdutoByIdProduto;

    @Id
    @Column(name = "id_receita_ingrediente", nullable = false)
    public long getIdReceitaIngrediente() {
        return idReceitaIngrediente;
    }

    public void setIdReceitaIngrediente(long idReceitaIngrediente) {
        this.idReceitaIngrediente = idReceitaIngrediente;
    }

    @Basic
    @Column(name = "id_receita", nullable = false)
    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    @Basic
    @Column(name = "id_produto", nullable = false)
    public long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(long idProduto) {
        this.idProduto = idProduto;
    }

    @Basic
    @Column(name = "quantidade", nullable = false)
    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    @Basic
    @Column(name = "medida_caseira", nullable = true, length = -1)
    public String getMedidaCaseira() {
        return medidaCaseira;
    }

    public void setMedidaCaseira(String medidaCaseira) {
        this.medidaCaseira = medidaCaseira;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbReceitaIngredienteEntity that = (TbReceitaIngredienteEntity) o;

        if (idReceitaIngrediente != that.idReceitaIngrediente) return false;
        if (idReceita != that.idReceita) return false;
        if (idProduto != that.idProduto) return false;
        if (quantidade != that.quantidade) return false;
        if (medidaCaseira != null ? !medidaCaseira.equals(that.medidaCaseira) : that.medidaCaseira != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idReceitaIngrediente ^ (idReceitaIngrediente >>> 32));
        result = 31 * result + idReceita;
        result = 31 * result + (int) (idProduto ^ (idProduto >>> 32));
        result = 31 * result + (int) (quantidade ^ (quantidade >>> 32));
        result = 31 * result + (medidaCaseira != null ? medidaCaseira.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_receita", referencedColumnName = "id_receita", nullable = false)
    public TbReceitaEntity getTbReceitaByIdReceita() {
        return tbReceitaByIdReceita;
    }

    public void setTbReceitaByIdReceita(TbReceitaEntity tbReceitaByIdReceita) {
        this.tbReceitaByIdReceita = tbReceitaByIdReceita;
    }

    @ManyToOne
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto", nullable = false)
    public TbProdutoEntity getTbProdutoByIdProduto() {
        return tbProdutoByIdProduto;
    }
    public void setTbProdutoByIdProduto(TbProdutoEntity tbProdutoByIdProduto) {
        this.tbProdutoByIdProduto = tbProdutoByIdProduto;
    }
}
