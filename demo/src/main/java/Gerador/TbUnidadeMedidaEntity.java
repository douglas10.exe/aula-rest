package Gerador;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tb_unidade_medida", schema = "cook_system", catalog = "postgres")
public class TbUnidadeMedidaEntity {
    private String descrição;
    private int idUnidademedida;
    private Collection<TbFichaTecnicaEntity> tbFichaTecnicasByIdUnidademedida;
    private Collection<TbProdutoEntity> tbProdutosByIdUnidademedida;
    private Collection<TbReceitaEntity> tbReceitasByIdUnidademedida;

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    @Id
    @Column(name = "id_unidademedida", nullable = false)
    public int getIdUnidademedida() {
        return idUnidademedida;
    }

    public void setIdUnidademedida(int idUnidademedida) {
        this.idUnidademedida = idUnidademedida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbUnidadeMedidaEntity that = (TbUnidadeMedidaEntity) o;

        if (idUnidademedida != that.idUnidademedida) return false;
        if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = descrição != null ? descrição.hashCode() : 0;
        result = 31 * result + idUnidademedida;
        return result;
    }

    @OneToMany(mappedBy = "tbUnidadeMedidaByIdUnidademedida")
    public Collection<TbFichaTecnicaEntity> getTbFichaTecnicasByIdUnidademedida() {
        return tbFichaTecnicasByIdUnidademedida;
    }

    public void setTbFichaTecnicasByIdUnidademedida(Collection<TbFichaTecnicaEntity> tbFichaTecnicasByIdUnidademedida) {
        this.tbFichaTecnicasByIdUnidademedida = tbFichaTecnicasByIdUnidademedida;
    }

    @OneToMany(mappedBy = "tbUnidadeMedidaByIdUnidademedida")
    public Collection<TbProdutoEntity> getTbProdutosByIdUnidademedida() {
        return tbProdutosByIdUnidademedida;
    }

    public void setTbProdutosByIdUnidademedida(Collection<TbProdutoEntity> tbProdutosByIdUnidademedida) {
        this.tbProdutosByIdUnidademedida = tbProdutosByIdUnidademedida;
    }

    @OneToMany(mappedBy = "tbUnidadeMedidaByIdUnidademedida")
    public Collection<TbReceitaEntity> getTbReceitasByIdUnidademedida() {
        return tbReceitasByIdUnidademedida;
    }

    public void setTbReceitasByIdUnidademedida(Collection<TbReceitaEntity> tbReceitasByIdUnidademedida) {
        this.tbReceitasByIdUnidademedida = tbReceitasByIdUnidademedida;
    }
}
