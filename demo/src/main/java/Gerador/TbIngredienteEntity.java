package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_ingrediente", schema = "cook_system", catalog = "postgres")
public class TbIngredienteEntity {
    private int idIngrediente;
    private double valorproduto;

    @Id
    @Column(name = "id_ingrediente", nullable = false)
    public int getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    @Basic
    @Column(name = "valorproduto", nullable = false, precision = 0)
    public double getValorproduto() {
        return valorproduto;
    }

    public void setValorproduto(double valorproduto) {
        this.valorproduto = valorproduto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbIngredienteEntity that = (TbIngredienteEntity) o;

        if (idIngrediente != that.idIngrediente) return false;
        if (Double.compare(that.valorproduto, valorproduto) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = idIngrediente;
        temp = Double.doubleToLongBits(valorproduto);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
