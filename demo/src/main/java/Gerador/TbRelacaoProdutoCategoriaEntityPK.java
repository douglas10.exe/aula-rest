package Gerador;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TbRelacaoProdutoCategoriaEntityPK implements Serializable {
    private int idCategoria;
    private int idProduto;

    @Column(name = "id_categoria", nullable = false)
    @Id
    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Column(name = "id_produto", nullable = false)
    @Id
    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbRelacaoProdutoCategoriaEntityPK that = (TbRelacaoProdutoCategoriaEntityPK) o;

        if (idCategoria != that.idCategoria) return false;
        if (idProduto != that.idProduto) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategoria;
        result = 31 * result + idProduto;
        return result;
    }
}
