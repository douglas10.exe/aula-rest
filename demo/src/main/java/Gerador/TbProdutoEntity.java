package Gerador;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tb_produto", schema = "cook_system", catalog = "postgres")
public class TbProdutoEntity {
    private int idProduto;
    private String descrição;
    private int volumeEmbalagem;
    private int valor;
    private int custoUnitario;
    private int idCategoriaProduto;
    private int idUnidademedida;
    private TbCategoriaProdutoEntity tbCategoriaProdutoByIdCategoriaProduto;
    private TbUnidadeMedidaEntity tbUnidadeMedidaByIdUnidademedida;
    private Collection<TbReceitaIngredienteEntity> tbReceitaIngredientesByIdProduto;

    @Id
    @Column(name = "id_produto", nullable = false)
    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    @Basic
    @Column(name = "volume_embalagem", nullable = false)
    public int getVolumeEmbalagem() {
        return volumeEmbalagem;
    }

    public void setVolumeEmbalagem(int volumeEmbalagem) {
        this.volumeEmbalagem = volumeEmbalagem;
    }

    @Basic
    @Column(name = "valor", nullable = false)
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Basic
    @Column(name = "custo_unitario", nullable = false)
    public int getCustoUnitario() {
        return custoUnitario;
    }

    public void setCustoUnitario(int custoUnitario) {
        this.custoUnitario = custoUnitario;
    }

    @Basic
    @Column(name = "id_categoria_produto", nullable = false)
    public int getIdCategoriaProduto() {
        return idCategoriaProduto;
    }

    public void setIdCategoriaProduto(int idCategoriaProduto) {
        this.idCategoriaProduto = idCategoriaProduto;
    }

    @Basic
    @Column(name = "id_unidademedida", nullable = false)
    public int getIdUnidademedida() {
        return idUnidademedida;
    }

    public void setIdUnidademedida(int idUnidademedida) {
        this.idUnidademedida = idUnidademedida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbProdutoEntity that = (TbProdutoEntity) o;

        if (idProduto != that.idProduto) return false;
        if (volumeEmbalagem != that.volumeEmbalagem) return false;
        if (valor != that.valor) return false;
        if (custoUnitario != that.custoUnitario) return false;
        if (idCategoriaProduto != that.idCategoriaProduto) return false;
        if (idUnidademedida != that.idUnidademedida) return false;
        if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idProduto;
        result = 31 * result + (descrição != null ? descrição.hashCode() : 0);
        result = 31 * result + volumeEmbalagem;
        result = 31 * result + valor;
        result = 31 * result + custoUnitario;
        result = 31 * result + idCategoriaProduto;
        result = 31 * result + idUnidademedida;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_categoria_produto", referencedColumnName = "id_categoriaproduto", nullable = false)
    public TbCategoriaProdutoEntity getTbCategoriaProdutoByIdCategoriaProduto() {
        return tbCategoriaProdutoByIdCategoriaProduto;
    }

    public void setTbCategoriaProdutoByIdCategoriaProduto(TbCategoriaProdutoEntity tbCategoriaProdutoByIdCategoriaProduto) {
        this.tbCategoriaProdutoByIdCategoriaProduto = tbCategoriaProdutoByIdCategoriaProduto;
    }

    @ManyToOne
    @JoinColumn(name = "id_unidademedida", referencedColumnName = "id_unidademedida", nullable = false)
    public TbUnidadeMedidaEntity getTbUnidadeMedidaByIdUnidademedida() {
        return tbUnidadeMedidaByIdUnidademedida;
    }

    public void setTbUnidadeMedidaByIdUnidademedida(TbUnidadeMedidaEntity tbUnidadeMedidaByIdUnidademedida) {
        this.tbUnidadeMedidaByIdUnidademedida = tbUnidadeMedidaByIdUnidademedida;
    }

    @OneToMany(mappedBy = "tbProdutoByIdProduto")
    public Collection<TbReceitaIngredienteEntity> getTbReceitaIngredientesByIdProduto() {
        return tbReceitaIngredientesByIdProduto;
    }

    public void setTbReceitaIngredientesByIdProduto(Collection<TbReceitaIngredienteEntity> tbReceitaIngredientesByIdProduto) {
        this.tbReceitaIngredientesByIdProduto = tbReceitaIngredientesByIdProduto;
    }
}
