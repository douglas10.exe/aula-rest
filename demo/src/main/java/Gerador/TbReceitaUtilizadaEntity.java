package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_receita_utilizada", schema = "cook_system", catalog = "postgres")
public class TbReceitaUtilizadaEntity {
    private long idReceitaUtilizada;
    private int idReceita;
    private int quantidade;
    private TbReceitaEntity tbReceitaByIdReceita;

    @Id
    @Column(name = "id_receita_utilizada", nullable = false)
    public long getIdReceitaUtilizada() {
        return idReceitaUtilizada;
    }

    public void setIdReceitaUtilizada(long idReceitaUtilizada) {
        this.idReceitaUtilizada = idReceitaUtilizada;
    }

    @Basic
    @Column(name = "id_receita", nullable = false)
    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    @Basic
    @Column(name = "quantidade", nullable = false)
    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbReceitaUtilizadaEntity that = (TbReceitaUtilizadaEntity) o;

        if (idReceitaUtilizada != that.idReceitaUtilizada) return false;
        if (idReceita != that.idReceita) return false;
        if (quantidade != that.quantidade) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idReceitaUtilizada ^ (idReceitaUtilizada >>> 32));
        result = 31 * result + idReceita;
        result = 31 * result + quantidade;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_receita", referencedColumnName = "id_receita", nullable = false)
    public TbReceitaEntity getTbReceitaByIdReceita() {
        return tbReceitaByIdReceita;
    }

    public void setTbReceitaByIdReceita(TbReceitaEntity tbReceitaByIdReceita) {
        this.tbReceitaByIdReceita = tbReceitaByIdReceita;
    }
}
