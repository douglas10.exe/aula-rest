package Gerador;

import javax.persistence.*;

@Entity
@Table(name = "tb_modo_preparo_receita", schema = "cook_system", catalog = "postgres")
public class TbModoPreparoReceitaEntity {
    private long idReceitaModoPreparo;
    private String descrição;
    private int idReceita;
    private TbReceitaEntity tbReceitaByIdReceita;

    @Basic
    @Column(name = "id_receita_modo_preparo", nullable = false)
    public long getIdReceitaModoPreparo() {
        return idReceitaModoPreparo;
    }

    public void setIdReceitaModoPreparo(long idReceitaModoPreparo) {
        this.idReceitaModoPreparo = idReceitaModoPreparo;
    }

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    @Basic
    @Column(name = "id_receita", nullable = false)
    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TbModoPreparoReceitaEntity that = (TbModoPreparoReceitaEntity) o;

        if (idReceitaModoPreparo != that.idReceitaModoPreparo) return false;
        if (idReceita != that.idReceita) return false;
        if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idReceitaModoPreparo ^ (idReceitaModoPreparo >>> 32));
        result = 31 * result + (descrição != null ? descrição.hashCode() : 0);
        result = 31 * result + idReceita;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_receita", referencedColumnName = "id_receita", nullable = false)
    public TbReceitaEntity getTbReceitaByIdReceita() {
        return tbReceitaByIdReceita;
    }

    public void setTbReceitaByIdReceita(TbReceitaEntity tbReceitaByIdReceita) {
        this.tbReceitaByIdReceita = tbReceitaByIdReceita;
    }
}
