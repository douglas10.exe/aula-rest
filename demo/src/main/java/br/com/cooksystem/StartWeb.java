package br.com.cooksystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

//        teste
@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.*"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.*")
//@RestController
//@RequestMapping("rest/cliente")
public class StartWeb {

    public static void main(String[] args) {
        SpringApplication.run(StartWeb.class, args);
    }
    }
