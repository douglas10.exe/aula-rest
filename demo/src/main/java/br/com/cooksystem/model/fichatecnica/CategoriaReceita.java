package br.com.cooksystem.model.fichatecnica;


import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "tb_categoria_receita", schema = "cook_system", catalog = "postgres")
public class CategoriaReceita {
    @SequenceGenerator(name = "categoria_receita_sequence", sequenceName = "cook_system.tb_categoriareceita_id_categoriareceita_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categoria_receita_sequence")
    @Id
    @Column(name = "id_categoriareceita")
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "descricao")
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    @OneToMany(mappedBy = "categoriaReceita", fetch = FetchType.LAZY )
    private Collection<Receita> receitas;
    public Collection<Receita> getReceitas() {
        return receitas;
    }

    public void setReceitas(Collection<Receita> receitas) {
        this.receitas = receitas;
    }



}
