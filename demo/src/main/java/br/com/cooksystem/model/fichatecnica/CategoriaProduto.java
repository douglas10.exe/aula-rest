package br.com.cooksystem.model.fichatecnica;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Collection;


@Entity
@Table(name = "tb_categoria_produto", schema = "cook_system", catalog = "postgres")
public class CategoriaProduto {
    private int idCategoriaproduto;
    private String descrição;
    private long idProduto;
    private Collection<Produto> produtos;
    //private Collection<TbReceitaIngredienteEntity> tbReceitaIngredientesByIdCategoriaproduto;

    @SequenceGenerator(name = "produto_sequence", sequenceName = "cook_system.tb_categoriaproduto_id_categoriaproduto_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produto_sequence")
    @Id
    @Column(name = "id_categoriaproduto", nullable = false)
    public int getIdCategoriaproduto() {
        return idCategoriaproduto;
    }

    public void setIdCategoriaproduto(int idCategoriaproduto) {
        this.idCategoriaproduto = idCategoriaproduto;
    }

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gerador.TbCategoriaProdutoEntity that = (Gerador.TbCategoriaProdutoEntity) o;

        // if (idCategoriaproduto != that.idCategoriaproduto) return false;
        ////  if (idProduto != that.idProduto) return false;
        // if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategoriaproduto;
        result = 31 * result + (descrição != null ? descrição.hashCode() : 0);
        result = 31 * result + (int) (idProduto ^ (idProduto >>> 32));
        return result;
    }

    // @jsonIgnore
    //@Transactional
    //@OneToMany(fetch = FetchType.LAZY)
    @OneToMany(mappedBy = "categoriaProduto", fetch = FetchType.EAGER)
    public Collection<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(Collection<Produto> produtos) {
        this.produtos = produtos;
    }
     /*
    @OneToMany(mappedBy = "tbCategoriaProdutoByIdProduto")
    public Collection<TbReceitaIngredienteEntity> getTbReceitaIngredientesByIdCategoriaproduto() {
        return tbReceitaIngredientesByIdCategoriaproduto;
    }

    public void setTbReceitaIngredientesByIdCategoriaproduto(Collection<TbReceitaIngredienteEntity> tbReceitaIngredientesByIdCategoriaproduto) {
        this.tbReceitaIngredientesByIdCategoriaproduto = tbReceitaIngredientesByIdCategoriaproduto;*/

}
