package br.com.cooksystem.model.fichatecnica;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_unidade_medida", schema = "cook_system", catalog = "postgres")
public class UnidadeMedida {
    private String descrição;
    private int idUnidademedida;
    private Collection<Produto> produtos;
    private Collection<Receita> receitas;

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }
    @Id
    @Column(name = "id_unidademedida", nullable = false)
    public int getIdUnidademedida() {
        return idUnidademedida;
    }

    public void setIdUnidademedida(int idUnidademedida) {
        this.idUnidademedida = idUnidademedida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnidadeMedida that = (UnidadeMedida) o;
        return idUnidademedida == that.idUnidademedida &&
                Objects.equals(descrição, that.descrição);
    }

    @OneToMany(mappedBy = "unidadeMedida", fetch = FetchType.LAZY )
    public Collection<Produto> getProdutos() {
        return produtos;
    }
    public void setProdutos(Collection<Produto> produtos) {
        this.produtos = produtos;
    }

    @OneToMany(mappedBy = "unidadeMedidaReceita", fetch = FetchType.LAZY )
    public Collection<Receita> getReceitas() {
        return receitas;
    }
    public void setReceitas(Collection<Receita> receitas) {
        this.receitas = receitas;
    }
    @Override
    public int hashCode() {
        return Objects.hash(descrição, idUnidademedida);
    }


}
