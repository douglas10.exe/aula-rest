package br.com.cooksystem.model.fichatecnica;


import br.com.cooksystem.model.fichatecnica.Produto;
import br.com.cooksystem.model.fichatecnica.Receita;

import javax.persistence.*;


@Entity
@Table(name = "tb_receita_ingrediente", schema = "cook_system", catalog = "postgres")
public class ReceitaIngrediente {
    private long idReceitaIngrediente;
    private long quantidade;
    private String medidaCaseira;
    private Receita receita;
    private  Produto produto;

    @SequenceGenerator(name = "receita_ingrediente_sequence", sequenceName = "cook_system.tb_receita_ingrediente_id_receita_ingrediente_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "receita_ingrediente_sequence")
    @Id
    @Column(name = "id_receita_ingrediente", nullable = false)
    public long getIdReceitaIngrediente() {
        return idReceitaIngrediente;
    }

    public void setIdReceitaIngrediente(long idReceitaIngrediente) {
        this.idReceitaIngrediente = idReceitaIngrediente;
    }


    @Basic
    @Column(name = "quantidade", nullable = false)
    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    @Basic
    @Column(name = "medida_caseira", nullable = true, length = -1)
    public String getMedidaCaseira() {
        return medidaCaseira;
    }

    public void setMedidaCaseira(String medidaCaseira) {
        this.medidaCaseira = medidaCaseira;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idReceitaIngrediente ^ (idReceitaIngrediente >>> 32));
      //  result = 31 * result + idReceita;
       // result = 31 * result + (int) (idProduto ^ (idProduto >>> 32));
        result = 31 * result + (int) (quantidade ^ (quantidade >>> 32));
        result = 31 * result + (medidaCaseira != null ? medidaCaseira.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_receita",referencedColumnName = "id_receita", nullable = false)
     public Receita getReceita() {
        return receita;
    }

    public void setReceita(Receita receita) {
        this.receita = receita;
    }
    @ManyToOne
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto", nullable = false)
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }


}
