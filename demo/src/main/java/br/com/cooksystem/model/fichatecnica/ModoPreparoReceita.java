package br.com.cooksystem.model.fichatecnica;

import Gerador.TbModoPreparoReceitaEntity;

import javax.persistence.*;


@Entity
@Table(name = "tb_modo_preparo_receita", schema = "cook_system", catalog = "postgres")
public class ModoPreparoReceita {

    private long idReceitaModoPreparo;
    private String descrição;
    private int idReceita;
    private Receita receita;

    @ManyToOne
    @JoinColumn(name = "id_receita", referencedColumnName = "id_receita", nullable = false)
    public Receita getReceita() {
        return receita;
    }

    public void setReceita(Receita receita) {
        this.receita = receita;
    }

    @SequenceGenerator(name = "modo_preparo_receita_sequence", sequenceName = "cook_system.tb_modo_preparo_receita_id_receita_modo_preparo_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modo_preparo_receita_sequence")
    @Id
    @Column(name = "id_receita_modo_preparo", nullable = false)
    public long getIdReceitaModoPreparo() {
        return idReceitaModoPreparo;
    }

    public void setIdReceitaModoPreparo(long idReceitaModoPreparo) {
        this.idReceitaModoPreparo = idReceitaModoPreparo;
    }

    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

       // TbModoPreparoReceitaEntity that = (TbModoPreparoReceitaEntity) o;

        //if (idReceitaModoPreparo != that.idReceitaModoPreparo) return false;
        //if (idReceita != that.idReceita) return false;
        //if (descrição != null ? !descrição.equals(that.descrição) : that.descrição != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idReceitaModoPreparo ^ (idReceitaModoPreparo >>> 32));
        result = 31 * result + (descrição != null ? descrição.hashCode() : 0);
        result = 31 * result + idReceita;
        return result;
    }

}
