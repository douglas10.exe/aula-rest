package br.com.cooksystem.model.fichatecnica;



import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;


@Entity(name = "Produto")
@Table(name = "tb_produto", schema = "cook_system")
public class Produto {
    @SequenceGenerator(name = "produto_sequence", sequenceName = "cook_system.tb_produto_id_produto_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produto_sequence")
    @Id
    @Column(name = "id_produto")
    private int idProduto;
    @Basic
    @Column(name = "descrição", nullable = false, length = -1)
    private String descrição;
    @Basic
    @Column(name = "volume_embalagem", nullable = false)
    private double volumeEmbalagem;
    @Basic
    @Column(name = "valor", nullable = false)
    private double valor;
    @Basic
    @Column(name = "custo_unitario", nullable = false)
    private double custoUnitario;
    @ManyToOne
    @JoinColumn(name = "id_unidademedida", referencedColumnName = "id_unidademedida", nullable = false)
    private UnidadeMedida unidadeMedida;
    @ManyToOne
    @JoinColumn(name = "id_categoria_produto", referencedColumnName = "id_categoriaproduto", nullable = false)
    private CategoriaProduto categoriaProduto ;
    @OneToMany(mappedBy = "produto")
    private Collection<ReceitaIngrediente>  receitaIngredientes ;

    public Collection<ReceitaIngrediente> getReceitaIngredientes() {
        return receitaIngredientes;
    }

    public void setReceitaIngredientes(Collection<ReceitaIngrediente> receitaIngredientes) {
        this.receitaIngredientes = receitaIngredientes;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public CategoriaProduto getCategoriaProduto() {
        return categoriaProduto;
    }

    public void setCategoriaProduto(CategoriaProduto categoriaProduto) {
        this.categoriaProduto = categoriaProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    public double getVolumeEmbalagem() {
        return volumeEmbalagem;
    }

    public void setVolumeEmbalagem(double volumeEmbalagem) {
        this.volumeEmbalagem = volumeEmbalagem;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    public double getCustoUnitario() {
        return custoUnitario;
    }

    public void setCustoUnitario(double custoUnitario) {
        this.custoUnitario = custoUnitario;
    }/*
    @Basic
    @Column(name = "id_categoria_produto", nullable = false)
    public int getIdCategoriaProduto() {
        return idCategoriaProduto;
    }

    public void setIdCategoriaProduto(int idCategoriaProduto) {
        this.idCategoriaProduto = idCategoriaProduto;
    }*/

    public UnidadeMedida getUnidademedida() {
        return unidadeMedida;
    }

    public void setUnidademedida(UnidadeMedida unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produto that = (Produto) o;
        return idProduto == that.idProduto &&
                volumeEmbalagem == that.volumeEmbalagem &&
                valor == that.valor &&
                custoUnitario == that.custoUnitario &&
                //idCategoriaProduto == that.idCategoriaProduto &&
                unidadeMedida == that.unidadeMedida &&
                Objects.equals(descrição, that.descrição);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProduto, descrição, volumeEmbalagem, valor, custoUnitario, /*idCategoriaProduto,*/ unidadeMedida);
    }
    /*
    @ManyToOne
    @JoinColumn(name = "id_categoria_produto", referencedColumnName = "id_categoriaproduto", nullable = false)
    public TbCategoriaProdutoEntity getTbCategoriaProdutoByIdCategoriaProduto() {
        return tbCategoriaProdutoByIdCategoriaProduto;
    }

    public void setTbCategoriaProdutoByIdCategoriaProduto(TbCategoriaProdutoEntity tbCategoriaProdutoByIdCategoriaProduto) {
        this.tbCategoriaProdutoByIdCategoriaProduto = tbCategoriaProdutoByIdCategoriaProduto;
    }*/
     /*
    public UnidadeMedida getTbUnidadeMedidaByIdUnidademedida() {
        return tbUnidadeMedidaByIdUnidademedida;
    }

    public void setTbUnidadeMedidaByIdUnidademedida(UnidadeMedida tbUnidadeMedidaByIdUnidademedida) {
        this.tbUnidadeMedidaByIdUnidademedida = tbUnidadeMedidaByIdUnidademedida;
    }*/
}
