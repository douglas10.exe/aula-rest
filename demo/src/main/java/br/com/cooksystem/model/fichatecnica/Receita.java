package br.com.cooksystem.model.fichatecnica;


import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "tb_receita", schema = "cook_system", catalog = "postgres")
public class Receita {

    private String nome;
    private int rendimento;
    private int idUnidademedida;
    private int idCategoriareceita;
    private int idReceita;
    private UnidadeMedida unidadeMedidaReceita;
    private CategoriaReceita categoriaReceita;
    private Collection<ReceitaIngrediente> receitaIngrediente;
    private Collection<ModoPreparoReceita> modoPreparoReceita;

    @Basic
    @Column(name = "nome", nullable = false, length = -1)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Basic
    @Column(name = "rendimento", nullable = false)
    public int getRendimento() {
        return rendimento;
    }

    public void setRendimento(int rendimento) {
        this.rendimento = rendimento;
    }

    @ManyToOne
    @JoinColumn(name = "id_unidademedida", referencedColumnName = "id_unidademedida", nullable = false)
    public UnidadeMedida getUnidadeMedidaReceita() {
        return unidadeMedidaReceita;
    }

    public void setUnidadeMedidaReceita(UnidadeMedida unidadeMedidaReceita) {
        this.unidadeMedidaReceita = unidadeMedidaReceita;
    }

    @ManyToOne
    @JoinColumn(name = "id_categoriareceita", referencedColumnName = "id_categoriareceita", nullable = false)
    public CategoriaReceita getCategoriaReceita() {
        return categoriaReceita;
    }

    public void setCategoriaReceita(CategoriaReceita categoriaReceita) {
        this.categoriaReceita = categoriaReceita;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "receita")
    public Collection<ReceitaIngrediente> getReceitaIngrediente() {
        return receitaIngrediente;
    }

    public void setReceitaIngrediente(Collection<ReceitaIngrediente> receitaIngrediente) {
        this.receitaIngrediente = receitaIngrediente;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "receita")
    public Collection<ModoPreparoReceita> getModoPreparoReceita() {
        return modoPreparoReceita;
    }

    public void setModoPreparoReceita(Collection<ModoPreparoReceita> modoPreparoReceita) {
        this.modoPreparoReceita = modoPreparoReceita;
    }

    @SequenceGenerator(name = "receita_sequence", sequenceName = "cook_system.tb_receita_id_receita_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "receita_sequence")
    @Id
    @Column(name = "id_receita", nullable = false)
    public int getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(int idReceita) {
        this.idReceita = idReceita;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gerador.TbReceitaEntity that = (Gerador.TbReceitaEntity) o;

        //if (rendimento != that.rendimento) return false;
        //if (idUnidademedida != that.idUnidademedida) return false;
        //if (idCategoriareceita != that.idCategoriareceita) return false;
        //if (idReceita != that.idReceita) return false;
        // if (nome != null ? !nome.equals(that.nome) : that.nome != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nome != null ? nome.hashCode() : 0;
        result = 31 * result + rendimento;
        result = 31 * result + idUnidademedida;
        result = 31 * result + idCategoriareceita;
        result = 31 * result + idReceita;
        return result;
    }

   /*
    @OneToMany(mappedBy = "tbReceitaByIdReceita")
    public Collection<TbModoPreparoReceitaEntity> getTbModoPreparoReceitasByIdReceita() {
        return tbModoPreparoReceitasByIdReceita;
    }

    public void setTbModoPreparoReceitasByIdReceita(Collection<TbModoPreparoReceitaEntity> tbModoPreparoReceitasByIdReceita) {
        this.tbModoPreparoReceitasByIdReceita = tbModoPreparoReceitasByIdReceita;
    }

    @ManyToOne
    @JoinColumn(name = "id_unidademedida", referencedColumnName = "id_unidademedida", nullable = false)
    public TbUnidadeMedidaEntity getTbUnidadeMedidaByIdUnidademedida() {
        return tbUnidadeMedidaByIdUnidademedida;
    }

    public void setTbUnidadeMedidaByIdUnidademedida(TbUnidadeMedidaEntity tbUnidadeMedidaByIdUnidademedida) {
        this.tbUnidadeMedidaByIdUnidademedida = tbUnidadeMedidaByIdUnidademedida;
    }

    @ManyToOne
    @JoinColumn(name = "id_categoriareceita", referencedColumnName = "id_categoriareceita", nullable = false)
    public TbCategoriaReceitaEntity getTbCategoriaReceitaByIdCategoriareceita() {
        return tbCategoriaReceitaByIdCategoriareceita;
    }

    public void setTbCategoriaReceitaByIdCategoriareceita(TbCategoriaReceitaEntity tbCategoriaReceitaByIdCategoriareceita) {
        this.tbCategoriaReceitaByIdCategoriareceita = tbCategoriaReceitaByIdCategoriareceita;
    }

    @OneToMany(mappedBy = "tbReceitaByIdReceita")
    public Collection<TbReceitaIngredienteEntity> getTbReceitaIngredientesByIdReceita() {
        return tbReceitaIngredientesByIdReceita;
    }

    public void setTbReceitaIngredientesByIdReceita(Collection<TbReceitaIngredienteEntity> tbReceitaIngredientesByIdReceita) {
        this.tbReceitaIngredientesByIdReceita = tbReceitaIngredientesByIdReceita;
    }

    @OneToMany(mappedBy = "tbReceitaByIdReceita")
    public Collection<TbReceitaUtilizadaEntity> getTbReceitaUtilizadasByIdReceita() {
        return tbReceitaUtilizadasByIdReceita;
    }

    public void setTbReceitaUtilizadasByIdReceita(Collection<TbReceitaUtilizadaEntity> tbReceitaUtilizadasByIdReceita) {
        this.tbReceitaUtilizadasByIdReceita = tbReceitaUtilizadasByIdReceita;
    }*/
}
