package br.com.cooksystem.model.cliente;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity (name = "Usuario")
@Table(name = "tb_cadastro_usuario", schema = "cook_system")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @SequenceGenerator(name="pessoa_sequence", sequenceName="tb_cadastro_usuario_id_cadastro_seq", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pessoa_sequence")
    @Id
    @Column(name = "id_cadastro")
    private long id;
    @Column(name = "nome")
    private String nome;
    @Column(name = "email")
    private String email;
    @Column(name = "senha")
    private String senha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
