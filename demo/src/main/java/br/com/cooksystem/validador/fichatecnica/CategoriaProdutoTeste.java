package br.com.cooksystem.validador.fichatecnica;

import br.com.cooksystem.model.fichatecnica.CategoriaProduto;
import br.com.cooksystem.repository.fichatecnica.produto.CategoriaProdutoRepository;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.model.fichatecnica"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.repository")
//@ComponentScan(basePackages = "br.com.cooksystem.repository.acesso.ClientRepository")

public class CategoriaProdutoTeste {
    private static CategoriaProduto categoriaProduto;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(CategoriaProdutoTeste.class)
                .web(WebApplicationType.NONE)
                .run(args);

        try {
            System.out.println("começando");


            CategoriaProdutoRepository categoriaProdutoRepository = applicationContext.getBean(CategoriaProdutoRepository.class);

            for (int i = 0; i < 1; i++) {
                categoriaProduto = new CategoriaProduto();
                categoriaProduto.setDescrição("Arroz");
                categoriaProdutoRepository.salvar(categoriaProduto);
                System.out.println("fim..." + categoriaProduto.getIdCategoriaproduto());
            }
            categoriaProdutoRepository.lista();
            System.out.println("lista");

        }catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}