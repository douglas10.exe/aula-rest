package br.com.cooksystem.validador.fichatecnica;

import br.com.cooksystem.model.fichatecnica.CategoriaProduto;
import br.com.cooksystem.model.fichatecnica.Produto;
import br.com.cooksystem.model.fichatecnica.UnidadeMedida;
import br.com.cooksystem.repository.fichatecnica.produto.ProdutoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.model.fichatecnica"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.repository")
//@ComponentScan(basePackages = "br.com.cooksystem.repository.acesso.ClientRepository")


public class ProdutoTeste {

    private Produto produto;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(ProdutoTeste.class)
                .web(WebApplicationType.NONE)
                .run(args);

        try {
            System.out.println("começando");
            ObjectMapper mapper = new ObjectMapper();
            Produto p = new Produto();
            p.setCategoriaProduto(new CategoriaProduto());
            p.setUnidademedida(new UnidadeMedida());
            String jsonString = mapper.writeValueAsString(p);
            System.out.println(jsonString);


            ProdutoRepository produtoRepository = applicationContext.getBean(ProdutoRepository.class);

            for (int i = 0; i < 1; i++) {
                Produto produto = new Produto();
                produto.setDescrição("Azeite De Oliva Mesa 3Lt" + new Date());
                produto.setCustoUnitario(15.44);
                produto.setVolumeEmbalagem(3.00);
                produto.setValor(produto.getCustoUnitario() * produto.getVolumeEmbalagem());
                // ----------------------unidade medida
                UnidadeMedida unidadeMedida = new UnidadeMedida();
                unidadeMedida.setIdUnidademedida(1);
                produto.setUnidademedida(unidadeMedida);
                // ----------------------categoria produto
                CategoriaProduto CategoriaProduto = new CategoriaProduto();
                CategoriaProduto.setIdCategoriaproduto(1);
                produto.setCategoriaProduto(CategoriaProduto);
                produtoRepository.salvar(produto);
                System.out.println("Listando dados..." + produto.getIdProduto());


                List<Produto> lista = produtoRepository.lista();

                Iterator ite = lista.iterator();
                while (ite.hasNext()) {
                    Produto prc = (Produto) ite.next();
                    System.out.println("id " + prc.getIdProduto()+ "-" + " reg:" + prc.getDescrição());
                    //System.out.println(prc.getCategoriaProduto().getProdutos());
                    System.out.println("------");
                    //clientRepository.deletar(cli);
                }


                System.out.println("fim...");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}




