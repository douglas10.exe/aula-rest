package br.com.cooksystem.validador.fichatecnica;

import br.com.cooksystem.model.fichatecnica.ModoPreparoReceita;
import br.com.cooksystem.model.fichatecnica.Receita;
import br.com.cooksystem.repository.fichatecnica.receita.ModoPreparoReceitaRepository;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Iterator;
import java.util.List;


@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.model.fichatecnica"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.repository")
//@ComponentScan(basePackages = "br.com.cooksystem.repository.acesso.ClientRepository")

public class ModoPreparoReceitaTeste {
    private static ModoPreparoReceita modoPreparoReceita;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(ModoPreparoReceitaTeste.class)
                .web(WebApplicationType.NONE)
                .run(args);

        try {
            System.out.println("começando");
            ModoPreparoReceitaRepository modoPreparoReceitaRepository = applicationContext.getBean(ModoPreparoReceitaRepository.class);

            for (int i = 0; i < 1; i++) {
                modoPreparoReceita = new ModoPreparoReceita();
                modoPreparoReceita.setDescrição("2 colheres de sopa");
                //------------------------Receita
                Receita rc = new Receita();
                rc.setIdReceita(14);
                modoPreparoReceita.setReceita(rc);
                modoPreparoReceitaRepository.salvar(modoPreparoReceita);
                System.out.println("fim..." + modoPreparoReceita.getIdReceitaModoPreparo());
            }
            List<ModoPreparoReceita> lista = modoPreparoReceitaRepository.lista();

            Iterator ite = lista.iterator();
            while (ite.hasNext()) {
                ModoPreparoReceita md = (ModoPreparoReceita) ite.next();
                System.out.println("id " + md.getIdReceitaModoPreparo()+ "-" + " reg:" + md.getDescrição()+ " " + md.getReceita().getNome());
                System.out.println("------");
                //clientRepository.deletar(cli);
            }
            System.out.println("lista");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}