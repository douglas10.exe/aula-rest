package br.com.cooksystem.validador.fichatecnica;

import br.com.cooksystem.model.fichatecnica.*;
import br.com.cooksystem.repository.fichatecnica.receita.ReceitaIngredienteRepository;
import br.com.cooksystem.repository.fichatecnica.receita.ReceitaRepository;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Iterator;
import java.util.List;


@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.model.fichatecnica"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.repository")
//@ComponentScan(basePackages = "br.com.cooksystem.repository.acesso.ClientRepository")
public class ReceitaIngredienteTeste {

    private static ReceitaIngrediente receitaIngrediente;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(ReceitaIngredienteTeste.class)
                .web(WebApplicationType.NONE)
                .run(args);

        try {
            System.out.println("começando");


            ReceitaIngredienteRepository receitaIngredienteRepository = applicationContext.getBean(ReceitaIngredienteRepository.class);

            ReceitaRepository receitaRepository = applicationContext.getBean(ReceitaRepository.class);

                List<Receita> lista = receitaRepository.lista();

                Iterator ite = lista.iterator();
                while (ite.hasNext()) {
                    Receita rc = (Receita) ite.next();
                    System.out.println("id " + rc.getIdReceita() + "-" + " reg:" + rc.getNome() + " "+rc.getCategoriaReceita().getDescricao());
                    System.out.println("------");

                    receitaIngrediente = new ReceitaIngrediente();
                    //--------------------produto
                    Produto p = new Produto();
                    p.setIdProduto(118);
                    receitaIngrediente.setProduto(p);
                    //--------------------receita
                    Receita r = new Receita();
                    r.setIdReceita(17);
                    receitaIngrediente.setReceita(r);
                    receitaIngrediente.setQuantidade(10);
                    receitaIngrediente.setMedidaCaseira("10 colheres de sopa");

                    receitaIngredienteRepository.salvar(receitaIngrediente);

                }


                System.out.println("fim...");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
