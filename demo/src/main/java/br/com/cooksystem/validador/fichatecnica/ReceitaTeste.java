package br.com.cooksystem.validador.fichatecnica;

import br.com.cooksystem.model.fichatecnica.CategoriaReceita;
import br.com.cooksystem.model.fichatecnica.Receita;
import br.com.cooksystem.model.fichatecnica.UnidadeMedida;
import br.com.cooksystem.repository.fichatecnica.receita.ReceitaRepository;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Iterator;
import java.util.List;


@SpringBootApplication
//scanea a classe enty
@EntityScan(basePackages = {"br.com.cooksystem.model.fichatecnica"})
////scanea a classe repository
@ComponentScan(basePackages = "br.com.cooksystem.repository")
//@ComponentScan(basePackages = "br.com.cooksystem.repository.acesso.ClientRepository")
public class ReceitaTeste {

    private static Receita receita;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new SpringApplicationBuilder(ReceitaTeste.class)
                .web(WebApplicationType.NONE)
                .run(args);

        try {
            System.out.println("começando");


            ReceitaRepository receitaRepository = applicationContext.getBean(ReceitaRepository.class);

            for (int i = 0; i < 1; i++) {
                receita = new Receita();
                receita.setNome("frango caipira");
                //---------------Categoria receita
                CategoriaReceita categoriaReceita = new CategoriaReceita();
                categoriaReceita.setId(1);
                receita.setCategoriaReceita(categoriaReceita);
                //---------------unidade
                UnidadeMedida unidadeMedida = new UnidadeMedida();
                unidadeMedida.setIdUnidademedida(2);
                receita.setUnidadeMedidaReceita(unidadeMedida);
                receitaRepository.salvar(receita);
                System.out.println("Listando dados..." + receita.getIdReceita());


                List<Receita> lista = receitaRepository.lista();

                Iterator ite = lista.iterator();
                while (ite.hasNext()) {
                    Receita rc = (Receita) ite.next();
                    System.out.println("id " + rc.getIdReceita() + "-" + " reg:" + rc.getNome() + " "+rc.getCategoriaReceita().getDescricao());
                    System.out.println("------");
                    //clientRepository.deletar(cli);
                }


                System.out.println("fim...");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
