package br.com.cooksystem.web.controller.cliente;

import br.com.cooksystem.model.cliente.Usuario;
import br.com.cooksystem.model.fichatecnica.Produto;
import br.com.cooksystem.repository.fichatecnica.produto.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@RestController
public class ProdutoRestController {
    @Autowired
    private ProdutoRepository produtoRepository;

    @RequestMapping(path = "produto/employees", method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produto> getAllEmployees() {
        List<Produto> listClientes = produtoRepository.lista();
        for (Produto produto : listClientes) {
            System.out.println(produto.getDescrição());

        }
        //clientRepository = new ClientRepository();
        return produtoRepository.lista();
    }

    @PostMapping(value = "/produto/save", consumes = "application/json", produces = "application/json")
    // @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<Void> save(@RequestBody Produto produto) {

        produtoRepository.salvar(produto);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}