package br.com.cooksystem.web.controller.cliente;

import br.com.cooksystem.repository.cliente.ClientRepository;
import br.com.cooksystem.model.cliente.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class ClienteRestController {
    @Autowired
   private ClientRepository clientRepository;

    @RequestMapping(path="/employees", method= RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> getAllEmployees(){
        List<Usuario> listClientes = clientRepository.lista();
        for (Usuario cliente : listClientes ) {
            System.out.println(cliente.getNome());

        }
        //clientRepository = new ClientRepository();
        return clientRepository.lista();
    }
    @PostMapping(value="/save",consumes = "application/json", produces = "application/json")
  // @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<Void> save(@RequestBody Usuario usuario){

        clientRepository.salvar(usuario);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
