package br.com.cooksystem.repository.fichatecnica.produto;

import br.com.cooksystem.model.cliente.Usuario;
import br.com.cooksystem.model.fichatecnica.Produto;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ProdutoRepository {

    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<Produto> lista() {
        TypedQuery<Produto> query = manager.createQuery("from Produto", Produto.class);
        //manager.flush();
        return query.getResultList();
    }
    @Transactional
    public Produto salvar(Produto produto) {
        manager.persist(produto);
        System.out.println("ID "+produto.getIdProduto());
        //manager.flush();
        return produto;
    }


    public void atualizar(Produto produto) {
        manager.merge(produto);
    }
    ///@Transactional
    public void deletar(Produto produto) {
        System.out.println("Deletando ID "+produto.getIdProduto());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_produto WHERE id_produto = ?");
        query.setParameter(1,  + produto.getIdProduto());
        query.executeUpdate();
        //manager.flush();
    }


}
