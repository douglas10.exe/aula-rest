package br.com.cooksystem.repository.cliente;

import br.com.cooksystem.model.cliente.Usuario;
import org.springframework.stereotype.Repository;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ClientRepository {
    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<Usuario> lista() {
        TypedQuery<Usuario> query = manager.createQuery("from Usuario", Usuario.class);
        //manager.flush();
       return query.getResultList();
    }
    @Transactional
    public Usuario salvar(Usuario usuario) {
        manager.persist(usuario);
        System.out.println("ID "+usuario.getId());
        //manager.flush();
        return usuario;
    }


    public void atualizar(Usuario usuario) {
        manager.merge(usuario);
    }
    @Transactional
    public void deletar(Usuario usuario) {
        System.out.println("Deletando ID "+usuario.getId());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_cadastro_usuario WHERE id_cadastro = ?");
        query.setParameter(1,  + usuario.getId());
        query.executeUpdate();
        //manager.flush();
    }


}
