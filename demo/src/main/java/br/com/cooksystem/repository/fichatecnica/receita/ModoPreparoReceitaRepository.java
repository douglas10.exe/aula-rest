package br.com.cooksystem.repository.fichatecnica.receita;

import br.com.cooksystem.model.fichatecnica.ModoPreparoReceita;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository

public class ModoPreparoReceitaRepository {

    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<ModoPreparoReceita> lista() {
        TypedQuery<ModoPreparoReceita> query = manager.createQuery("from ModoPreparoReceita ", ModoPreparoReceita.class);
        //manager.flush();
        return query.getResultList();
    }

    @Transactional
    public ModoPreparoReceita salvar(ModoPreparoReceita modoPreparoReceita) {
        manager.persist(modoPreparoReceita);
        System.out.println("ID " + modoPreparoReceita.getIdReceitaModoPreparo());
        //manager.flush();
        return modoPreparoReceita;
    }


    public void atualizar(ModoPreparoReceita modoPreparoReceita) {
        manager.merge(modoPreparoReceita);
    }

    @Transactional
    public void deletar(ModoPreparoReceita modoPreparoReceita) {
        System.out.println("Deletando ID " + modoPreparoReceita.getIdReceitaModoPreparo());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_modo_preparo_receita WHERE id_receita_modo_preparo = ?");
        query.setParameter(1, +modoPreparoReceita.getIdReceitaModoPreparo());
        query.executeUpdate();
        //manager.flush();
    }


}