package br.com.cooksystem.repository.fichatecnica.receita;

import br.com.cooksystem.model.fichatecnica.CategoriaReceita;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;


@Repository
public class CategoriaReceitaRepository {
    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<CategoriaReceita> lista() {
        TypedQuery<CategoriaReceita> query = manager.createQuery("from CategoriaReceita c", CategoriaReceita.class);
        return query.getResultList();
    }
    @Transactional
    public CategoriaReceita salvar(CategoriaReceita categoriaReceita) {
            manager.persist(categoriaReceita);
        return categoriaReceita;
    }


    public void atualizar(CategoriaReceita categoriaReceita) {
        manager.merge(categoriaReceita);
    }

    public void deletar(CategoriaReceita categoriaReceita) {
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_categoriareceita WHERE id_categoriareceita = " + categoriaReceita.getId());
        query.executeUpdate();
    }
}