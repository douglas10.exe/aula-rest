package br.com.cooksystem.repository.fichatecnica.receita;

import br.com.cooksystem.model.cliente.Usuario;
import br.com.cooksystem.model.fichatecnica.Receita;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository

public class ReceitaRepository {

    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<Receita> lista() {
        TypedQuery<Receita> query = manager.createQuery("from Receita", Receita.class);
        //manager.flush();
        return query.getResultList();
    }

    @Transactional
    public Receita salvar(Receita receita) {
        manager.persist(receita);
        System.out.println("ID " + receita.getIdReceita());
        //manager.flush();
        return receita;
    }


    public void atualizar(Usuario receita) {
        manager.merge(receita);
    }

    @Transactional
    public void deletar(Receita receita) {
        System.out.println("Deletando ID " + receita.getIdReceita());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_receita WHERE id_receita = ?");
        query.setParameter(1, +receita.getIdReceita());
        query.executeUpdate();
        //manager.flush();
    }


}