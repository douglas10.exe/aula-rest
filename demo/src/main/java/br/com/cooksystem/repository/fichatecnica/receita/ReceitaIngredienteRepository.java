package br.com.cooksystem.repository.fichatecnica.receita;

import br.com.cooksystem.model.fichatecnica.ReceitaIngrediente;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;


@Repository
public class ReceitaIngredienteRepository {


    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<ReceitaIngrediente> lista() {
        TypedQuery<ReceitaIngrediente> query = manager.createQuery("from ReceitaIngrediente", ReceitaIngrediente.class);
        //manager.flush();
        return query.getResultList();
    }

    @Transactional
    public ReceitaIngrediente salvar(ReceitaIngrediente receitaIngrediente) {
        manager.persist(receitaIngrediente);
        System.out.println("ID " + receitaIngrediente.getIdReceitaIngrediente());
        //manager.flush();
        return receitaIngrediente;
    }


    public void atualizar(ReceitaIngrediente receitaIngrediente) {
        manager.merge(receitaIngrediente);
    }

    @Transactional
    public void deletar(ReceitaIngrediente receitaIngrediente) {
        System.out.println("Deletando ID " + receitaIngrediente.getIdReceitaIngrediente());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_receita_ingrediente WHERE id_receita_ingrediente = ?");
        query.setParameter(1, +receitaIngrediente.getIdReceitaIngrediente());
        query.executeUpdate();
        //manager.flush();
    }


}