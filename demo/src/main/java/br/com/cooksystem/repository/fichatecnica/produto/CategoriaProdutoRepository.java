package br.com.cooksystem.repository.fichatecnica.produto;

import br.com.cooksystem.model.fichatecnica.CategoriaProduto;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CategoriaProdutoRepository {

    @PersistenceContext
    private EntityManager manager;
    private EntityTransaction trc;

    public List<CategoriaProduto> lista() {
        TypedQuery<CategoriaProduto> query = manager.createQuery("from CategoriaProduto", CategoriaProduto.class);
        //manager.flush();
        return query.getResultList();
    }

    @Transactional
    public CategoriaProduto salvar(CategoriaProduto categoriaProduto) {
        manager.persist(categoriaProduto);
        System.out.println("ID " + categoriaProduto.getIdCategoriaproduto());
        //manager.flush();
        return categoriaProduto;
    }


    public void atualizar(CategoriaProduto categoriaProduto) {
        manager.merge(categoriaProduto);
    }

   @Transactional
    public void deletar(CategoriaProduto categoriaProduto) {
        System.out.println("Deletando ID " + categoriaProduto.getIdCategoriaproduto());
        Query query = manager.createNativeQuery("DELETE FROM cook_system.tb_categoria_produto WHERE id_categoriaprodutocategoriaProduto = ?");
        query.setParameter(1, +categoriaProduto.getIdCategoriaproduto());
        query.executeUpdate();
        //manager.flush();
    }


}
