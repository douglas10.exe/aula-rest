
--sql
-- DROP SCHEMA cook_system;

CREATE SCHEMA cook_system AUTHORIZATION postgres;

-- Drop table

-- DROP TABLE cook_system.tb_cadastro_usuario

CREATE TABLE cook_system.tb_cadastro_usuario (
                                                 id_cadastro serial NOT NULL,
                                                 nome varchar NOT NULL,
                                                 email varchar NOT NULL,
                                                 senha varchar NOT NULL,
                                                 CONSTRAINT tb_cadastro_usuario_pkey PRIMARY KEY (id_cadastro)
);

-- Drop table

-- DROP TABLE cook_system.tb_categoria_produto

CREATE TABLE cook_system.tb_categoria_produto (
                                                  id_categoriaproduto serial NOT NULL,
                                                  descrição varchar NOT NULL,
                                                  CONSTRAINT tb_categoriaproduto_pk PRIMARY KEY (id_categoriaproduto)
);

-- Drop table

-- DROP TABLE cook_system.tb_categoria_receita

CREATE TABLE cook_system.tb_categoria_receita (
                                                  id_categoriareceita bigserial NOT NULL,
                                                  descricao varchar(255) NULL,
                                                  CONSTRAINT tb_categoriareceita_pkey PRIMARY KEY (id_categoriareceita)
);

-- Drop table

-- DROP TABLE cook_system.tb_ficha_tecnica

CREATE TABLE cook_system.tb_ficha_tecnica (
                                              id_fichatecnica serial NOT NULL,
                                              rendimento int4 NOT NULL,
                                              id_unidademedida int4 NOT NULL,
                                              id_categoriareceita int4 NOT NULL,
                                              CONSTRAINT tb_fichatecnica_pk PRIMARY KEY (id_fichatecnica),
                                              CONSTRAINT tb_fichatecnica_tb_unidademedida_fk FOREIGN KEY (id_unidademedida) REFERENCES cook_system.tb_unidade_medida(id_unidademedida)
);

-- Drop table

-- DROP TABLE cook_system.tb_ingrediente

CREATE TABLE cook_system.tb_ingrediente (
                                            id_ingrediente serial NOT NULL,
                                            valorproduto float8 NOT NULL,
                                            CONSTRAINT tb_ingrediente_pk PRIMARY KEY (id_ingrediente)
);

-- Drop table

-- DROP TABLE cook_system.tb_modo_preparo_receita

CREATE TABLE cook_system.tb_modo_preparo_receita (
                                                     id_receita_modo_preparo bigserial NOT NULL,
                                                     descrição varchar NOT NULL,
                                                     id_receita int4 NOT NULL,
                                                     CONSTRAINT tb_modo_preparo_receita_tb_receita_fk FOREIGN KEY (id_receita) REFERENCES cook_system.tb_receita(id_receita)
);

-- Drop table

-- DROP TABLE cook_system.tb_produto

CREATE TABLE cook_system.tb_produto (
                                        id_produto serial NOT NULL,
                                        descrição varchar NOT NULL,
                                        volume_embalagem int4 NOT NULL,
                                        valor int4 NOT NULL,
                                        custo_unitario int4 NOT NULL,
                                        id_categoria_produto int4 NOT NULL,
                                        id_unidademedida int4 NOT NULL,
                                        CONSTRAINT tb_produto_pk PRIMARY KEY (id_produto),
                                        CONSTRAINT tb_produto_tb_categoriaproduto_fk FOREIGN KEY (id_categoria_produto) REFERENCES cook_system.tb_categoria_produto(id_categoriaproduto),
                                        CONSTRAINT tb_produto_tb_unidademedida_fk FOREIGN KEY (id_unidademedida) REFERENCES cook_system.tb_unidade_medida(id_unidademedida)
);

-- Drop table

-- DROP TABLE cook_system.tb_receita

CREATE TABLE cook_system.tb_receita (
                                        nome varchar NOT NULL,
                                        rendimento int4 NOT NULL,
                                        id_unidademedida int4 NOT NULL,
                                        id_categoriareceita int4 NOT NULL,
                                        id_receita serial NOT NULL,
                                        CONSTRAINT tb_receita_pk PRIMARY KEY (id_receita),
                                        CONSTRAINT tb_receita_tb_categoria_receita_fk FOREIGN KEY (id_categoriareceita) REFERENCES cook_system.tb_categoria_receita(id_categoriareceita),
                                        CONSTRAINT tb_receita_tb_unidademedida_fk FOREIGN KEY (id_unidademedida) REFERENCES cook_system.tb_unidade_medida(id_unidademedida)
);

-- Drop table

-- DROP TABLE cook_system.tb_receita_ingrediente

CREATE TABLE cook_system.tb_receita_ingrediente (
                                                    id_receita_ingrediente bigserial NOT NULL,
                                                    id_receita int4 NOT NULL,
                                                    id_produto int8 NOT NULL,
                                                    quantidade int8 NOT NULL,
                                                    medida_caseira varchar NULL,
                                                    CONSTRAINT tb_receita_ingrediente_pk PRIMARY KEY (id_receita_ingrediente),
                                                    CONSTRAINT tb_receita_ingrediente_tb_produto_fk FOREIGN KEY (id_produto) REFERENCES cook_system.tb_produto(id_produto),
                                                    CONSTRAINT tb_receita_ingrediente_tb_receita_fk FOREIGN KEY (id_receita) REFERENCES cook_system.tb_receita(id_receita)
);

-- Drop table

-- DROP TABLE cook_system.tb_receita_utilizada

CREATE TABLE cook_system.tb_receita_utilizada (
                                                  id_receita_utilizada bigserial NOT NULL,
                                                  id_receita int4 NOT NULL,
                                                  quantidade int4 NOT NULL,
                                                  CONSTRAINT tb_receita_utilizada_pk PRIMARY KEY (id_receita_utilizada),
                                                  CONSTRAINT tb_receita_utilizada_tb_receita_fk FOREIGN KEY (id_receita) REFERENCES cook_system.tb_receita(id_receita)
);

-- Drop table

-- DROP TABLE cook_system.tb_relacao_produto_categoria

CREATE TABLE cook_system.tb_relacao_produto_categoria (
                                                          id_categoria int4 NOT NULL,
                                                          id_produto int4 NOT NULL,
                                                          CONSTRAINT tb_relacao_produto_categoria_id_categoria_id_produto PRIMARY KEY (id_categoria, id_produto)
);

-- Drop table

-- DROP TABLE cook_system.tb_unidade_medida

CREATE TABLE cook_system.tb_unidade_medida (
                                               descrição varchar NOT NULL,
                                               id_unidademedida serial NOT NULL,
                                               CONSTRAINT tb_unidademedida_pk PRIMARY KEY (id_unidademedida)
);
