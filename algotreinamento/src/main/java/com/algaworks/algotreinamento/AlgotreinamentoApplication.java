package com.algaworks.algotreinamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgotreinamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgotreinamentoApplication.class, args);
	}

}
